package com.example.nico.recipetool;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by admin on 08.02.2018.
 */

@Dao
public interface RecipeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertRecipes(Recipe... recipes);

    @Delete
    public void deleteRecipes(Recipe... recipes);

    @Query("SELECT * FROM recipe")
    List<Recipe> getAllRecipe();

}
