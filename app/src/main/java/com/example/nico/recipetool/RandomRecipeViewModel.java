package com.example.nico.recipetool;

import android.arch.lifecycle.ViewModel;

/**
 * Created by admin on 06.02.2018.
 */

public class RandomRecipeViewModel extends ViewModel{
    private Recipe currentRecipe;

    public Recipe getCurrentRecipe(){
        if(currentRecipe == null){
           currentRecipe = RecipeRepository.getInstance().getRndRecipe();
        }
        return currentRecipe;
    }
    public void setCurrentRecipe(Recipe recipe){
        this.currentRecipe = recipe;
    }
}
