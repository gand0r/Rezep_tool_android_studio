package com.example.nico.recipetool;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class RecipeListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_list_activity);

        // Binding ListView
        ListView listView = ((ListView) findViewById(R.id.listRecipe));

        RecipeRepository recipeRepository = RecipeRepository.getInstance();

        ArrayAdapter<Recipe> adapter = new ArrayAdapter<Recipe>(this, android.R.layout.simple_list_item_1, android.R.id.text1, recipeRepository.getRecipes());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Recipe recipe = (Recipe) adapter.getItem(position);

                Intent intent = new Intent(RecipeListActivity.this, RecipeViewActivity.class);

                intent.putExtra("Recipe", recipe.getId());
                //based on item add info to intent
                startActivity(intent);
            }
        });

        if(!recipeRepository.getRecipes().isEmpty()) {
            FloatingActionButton rndBtn = (FloatingActionButton) findViewById(R.id.btnRndRecipe);
            rndBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(RecipeListActivity.this, RandomRecipeActivity.class));
                }
            });
        }
        FloatingActionButton createBtn = (FloatingActionButton) findViewById(R.id.btnDelRecipe);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RecipeListActivity.this, CreateRecipeActivity.class));
            }
        });
    }
}
