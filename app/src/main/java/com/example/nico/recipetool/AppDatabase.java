package com.example.nico.recipetool;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by admin on 08.02.2018.
 */
@Database(entities = {Recipe.class}, version=1)
public abstract class AppDatabase extends RoomDatabase{
    public abstract RecipeDao recipeDao();

}
