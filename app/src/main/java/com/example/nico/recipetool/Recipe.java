package com.example.nico.recipetool;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by admin on 06.02.2018.
 */
@Entity
public class Recipe {
    /**
     * variables of recipe
     */
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "title")
    public String recipeTitle;

    @ColumnInfo(name = "info")
    public String info;

    @ColumnInfo(name = "persons")
    public Integer persons;

    @ColumnInfo(name = "ingridients")
    public String ingredients;

    @ColumnInfo(name = "preperation")
    public String preparation;

    //Getter and Setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecipeTitle() {
        return recipeTitle;
    }

    public void setRecipeTitle(String recipeTitle) {
        this.recipeTitle = recipeTitle;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getPersons() {
        return persons;
    }

    public void setPersons(Integer persons) {
        this.persons = persons;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

//    public int id() {
//        return id;
//    }
//
//    public String getRecipeTitle() {
//        return recipeTitle;
//    }
//
//    public String getInfo() {
//        return info;
//    }
//
//    public int getPersons() {
//        return persons;
//    }
//
//    public String getIngredients() {
//        return ingredients;
//    }
//
//    public String getPreparation() {
//        return preparation;
//    }
//
////    public String getId() { return id; }
//
    @Override
    public String toString() {
        return "\n" + recipeTitle + "\n" + info + "\n";
    }



}
