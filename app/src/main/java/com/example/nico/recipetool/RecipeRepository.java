package com.example.nico.recipetool;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by admin on 06.02.2018.
 */

public class RecipeRepository {

    // Singleton
    private static RecipeRepository instance = null;

    synchronized public static RecipeRepository getInstance() {
        if (instance == null) {
            instance = new RecipeRepository();
        }
        return instance;
    }

    /**
     * Variables
     */
    List<Recipe> recipes;
    AppDatabase db;
    private Context appContext;

    /**
     * Constructor
     * init new ArrayList
     */
    private RecipeRepository() {
        recipes = new ArrayList<>();
    }

    private boolean isInitialized() {
        return appContext != null;
    }

    public void initialize(Context applicationContext) {
        appContext = applicationContext;
        db = Room.databaseBuilder(appContext,
                AppDatabase.class, "database-name").allowMainThreadQueries().build();

        getInstance().loadFromDB();
    }

    private void loadFromDB() {
        if (!isInitialized()) {
            Log.d("Initialized Error", "Error");
        }
        recipes = db.recipeDao().getAllRecipe();
    }

    public void createRecipe(int recipeId,String title, String info, int pers, String ingr, String prep) {
        Recipe recipe = new Recipe();

        if(recipeId != 0){
            recipe.setId(recipeId);
        }

        recipe.setRecipeTitle(title);
        recipe.setInfo(info);
        recipe.setPersons(pers);
        recipe.setIngredients(ingr);
        recipe.setPreparation(prep);
        db.recipeDao().insertRecipes(recipe);
        loadFromDB();
    }

    public void delRecipe(Recipe recipe) {
        db.recipeDao().deleteRecipes(recipe);
        loadFromDB();
    }

    /**
     * @return recipes
     */
    public List<Recipe> getRecipes() {
        return recipes;
    }

    public Recipe getRecipeById(int objectId) {
        for (Recipe recipe : recipes) {
            if (recipe.getId() == objectId) {
                return recipe;
            }
        }
        return null;
    }

    private static Random rnd = new Random();

    public Recipe getRndRecipe() {
        return recipes.get(rnd.nextInt(recipes.size()));
    }


}
