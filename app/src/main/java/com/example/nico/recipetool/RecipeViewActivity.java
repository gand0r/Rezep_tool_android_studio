package com.example.nico.recipetool;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class RecipeViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_view_activity);
        int objectId = getIntent().getIntExtra("Recipe", 0);

        Recipe recipe = RecipeRepository.getInstance().getRecipeById(objectId);

        // Declaration vaiables
        TextView recTitel, recInfo, recPers, recIng, recPrep;

        // Fill Variables
        recTitel = (TextView) findViewById(R.id.recTitle);
        recInfo = (TextView) findViewById(R.id.recInfo);
        recPers = (TextView) findViewById(R.id.recPers);
        recIng = (TextView) findViewById(R.id.recIng);
        recPrep = (TextView) findViewById(R.id.recPrep);

        recTitel.setText(recipe.getRecipeTitle());
        recInfo.setText(recipe.getInfo());
        recPers.setText("Für " + Integer.toString(recipe.getPersons()) + " Personen");
        recIng.setText(recipe.getIngredients());
        recPrep.setText(recipe.getPreparation());


        FloatingActionButton editBtn = (FloatingActionButton) findViewById(R.id.btnEditRecipe);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecipeViewActivity.this, CreateRecipeActivity.class);

                intent.putExtra("Recipe", recipe.getId());
                //based on item add info to intent
                startActivity(intent);
            }
        });

        FloatingActionButton delBtn = (FloatingActionButton) findViewById(R.id.btnDelRecipe);

        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecipeRepository.getInstance().delRecipe(recipe);
                Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(250);
                startActivity(new Intent(RecipeViewActivity.this, RecipeListActivity.class));
            }
        });

    }
}
