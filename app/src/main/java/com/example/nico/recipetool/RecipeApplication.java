package com.example.nico.recipetool;

import android.app.Application;

/**
 * Created by admin on 08.02.2018.
 */

public class RecipeApplication extends Application {
    @Override
    public void onCreate() {
        RecipeRepository.getInstance().initialize(getApplicationContext());
        super.onCreate();
    }
}
