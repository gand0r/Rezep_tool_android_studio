package com.example.nico.recipetool;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RandomRecipeActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 200;


    RecipeRepository recipeRepository = RecipeRepository.getInstance();

    RandomRecipeViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.random_recipe_activity);

        model = ViewModelProviders.of(this).get(RandomRecipeViewModel.class);

        showNewRecipe(model.getCurrentRecipe());

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        FloatingActionButton rndBtn = (FloatingActionButton) findViewById(R.id.btnDelRecipe);
        FloatingActionButton nextRndBtn = (FloatingActionButton) findViewById(R.id.btnRndRec);

        rndBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Recipe recipe = model.getCurrentRecipe();
                Intent intent = new Intent(RandomRecipeActivity.this, RecipeViewActivity.class);

                intent.putExtra("Recipe", recipe.getId());
                //based on item add info to intent
                startActivity(intent);
            }
        });

        MediaPlayer mediaPlayer = MediaPlayer.create(RandomRecipeActivity.this, R.raw.laser_blast);

        nextRndBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
                showNewRecipe(getNewRecipe());
            }
        });


    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 500) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
                // speed shacking > 600
                if (speed > SHAKE_THRESHOLD) {
                    showNewRecipe(getNewRecipe());
                }

                last_y = y;
                last_x = x;
                last_z = z;

            }
        }
    }

    public Recipe getNewRecipe() {
        Recipe rndRecipe = recipeRepository.getRndRecipe();
        model.setCurrentRecipe(rndRecipe);
        return rndRecipe;
    }

    public void showNewRecipe(Recipe recipe) {
        // Variables
        TextView recTitle, recInfo;

        recTitle = ((TextView) findViewById(R.id.recTitle));
        recInfo = ((TextView) findViewById(R.id.recInfo));

        recTitle.setText(recipe.getRecipeTitle());
        recInfo.setText(recipe.getInfo());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

}
