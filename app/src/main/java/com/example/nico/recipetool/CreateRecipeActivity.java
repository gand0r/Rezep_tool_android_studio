package com.example.nico.recipetool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_recipe_activity);

        RecipeRepository recipeRepository = RecipeRepository.getInstance();

        // Handle new Recipe
        final Button button = ((Button) findViewById(R.id.newSafe));
        final EditText recTitle = ((EditText) findViewById(R.id.newTitle));
        final EditText recInfo = ((EditText) findViewById(R.id.newInfo));
        final EditText recPers = ((EditText) findViewById(R.id.newPers));
        final EditText recIngr = ((EditText) findViewById(R.id.newIngridient));
        final EditText recPrep = ((EditText) findViewById(R.id.newPrep));

        Recipe recipe = recipeRepository.getRecipeById(getIntent().getIntExtra("Recipe", 0));

        if (recipe != null) {
            recTitle.setText(recipe.getRecipeTitle());
            recInfo.setText(recipe.getInfo());
            recPers.setText(recipe.getPersons().toString());
            recIngr.setText(recipe.getIngredients());
            recPrep.setText(recipe.getPreparation());
        }

        button.setOnClickListener(btn -> {

            int recipeId = 0;
            if (getIntent().hasExtra("Recipe")) {
                recipeId = recipe.getId();
            }

            String title = recTitle.getText().toString();
            String info = recInfo.getText().toString();
            String ingr = recIngr.getText().toString();
            String prep = recPrep.getText().toString();

            if (TextUtils.isEmpty(title)) {
                Toast.makeText(CreateRecipeActivity.this, "Der Titel darf nicht leer sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }else if(TextUtils.isEmpty(info)){
                Toast.makeText(CreateRecipeActivity.this, "Die Info darf nicht leer sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }else if(TextUtils.isEmpty(recPers.getText().toString())){
                Toast.makeText(CreateRecipeActivity.this, "Anzahl Personen darf nicht leer sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }else if(Integer.parseInt(recPers.getText().toString())== 0){
                Toast.makeText(CreateRecipeActivity.this, "Anzahl Personen darf nicht 0 sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }else if(TextUtils.isEmpty(ingr)){
                Toast.makeText(CreateRecipeActivity.this, "Die Zutaten dürfen nicht leer sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }else if(TextUtils.isEmpty(prep)){
                Toast.makeText(CreateRecipeActivity.this, "Die Zubereitung darf nicht leer sein!!!", Toast.LENGTH_SHORT).show();
                return;
            }
            int pers = Integer.parseInt(recPers.getText().toString());

            recipeRepository.createRecipe(recipeId, title, info, pers, ingr, prep);

            startActivity(new Intent(CreateRecipeActivity.this, RecipeListActivity.class));
        });
    }
}

